package com.jwtexample.jwtdemo.controller;

import com.jwtexample.jwtdemo.domain.Record;
import com.jwtexample.jwtdemo.repository.RecordRepository;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/record")
public class RecordController {

    private RecordRepository recordRepository;

    public RecordController(RecordRepository recordRepository) {
        this.recordRepository = recordRepository;
    }

    @PostMapping("/save")
    private void saveRecord(@RequestBody Record record) {
        recordRepository.save(record);
    }

    @PutMapping("/{id}")
    private void editRecord(@PathVariable long id, @RequestBody Record record) {
        Record existsRecord = recordRepository.findOne(id);
        Assert.notNull(existsRecord, "Record must exists.");
        existsRecord.setText(record.getText());

        recordRepository.save(record);
    }
}
