package com.jwtexample.jwtdemo.repository;

import com.jwtexample.jwtdemo.domain.Record;
import org.springframework.data.repository.CrudRepository;

public interface RecordRepository extends CrudRepository<Record, Long> {
//    Optional<Record> findOne(Long id);
}
