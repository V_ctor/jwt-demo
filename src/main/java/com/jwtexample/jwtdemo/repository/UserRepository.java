package com.jwtexample.jwtdemo.repository;

import com.jwtexample.jwtdemo.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
