package security;

public class Constants {
    public static final String SECRET_KEY = "SecretKey";
    public static final long EXPIRATION_TIME = 7*24*60*60*1000; // 10 days
    public static final String TOKEN_PREFIX = "jwt_prefix ";
    public static final String JWT_HEADER = "JwtAuthorization";
    public static final String SIGN_UP_URL = "/users/sign-up";
}
