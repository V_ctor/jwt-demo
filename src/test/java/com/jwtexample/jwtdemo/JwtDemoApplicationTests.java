package com.jwtexample.jwtdemo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jwtexample.jwtdemo.domain.Record;
import com.jwtexample.jwtdemo.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static security.Constants.JWT_HEADER;

//import org.junit.Before;
//import org.junit.Test;


//@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class JwtDemoApplicationTests extends AbstractTransactionalTestNGSpringContextTests {
    private static final String USER_NAME = "user_name";
    private static final String RAW_PASSWORD = "password";
    final private User user = new User().setUsername(USER_NAME).setPassword(RAW_PASSWORD);
    final private Record record = new Record().setText("Some text");
    private String userAsJson;
    private String recordAsJson;

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;


    @BeforeClass
    public void setup() throws JsonProcessingException {
        userAsJson = objectMapper.writeValueAsString(user);
        recordAsJson = objectMapper.writeValueAsString(record);
    }

    @Test
    public void loginWithNoUsersWillFail() throws Exception {
        login()
            .andExpect(status().isUnauthorized());
    }

    @Test(dependsOnMethods = "loginWithNoUsersWillFail")
    public void signinUser() throws Exception {
        signin()
            .andExpect(status().isOk());
    }

    @Test(dependsOnMethods = "signinUser")
    public void loginWithSignedinUser() throws Exception {
        signin();
        login()
            .andExpect(status().isOk());
    }

    @Test(dependsOnMethods = "signinUser")
    public void saveRecordWithoutJwtWillFail() throws Exception {
        mockMvc.perform(post("/record/save")
            .contentType(MediaType.APPLICATION_JSON)
            .content(recordAsJson))
            .andExpect(status().isForbidden());
    }

    @Test(dependsOnMethods = "signinUser")
    public void saveRecordWithoJwtWillSuccess() throws Exception {
        signin();
        ResultActions resultLogin = login();
        resultLogin
            .andDo(print());
        final String jwt = resultLogin.andReturn().getResponse().getHeader(JWT_HEADER);

        mockMvc.perform(post("/record/save")
            .contentType(MediaType.APPLICATION_JSON)
            .content(recordAsJson)
            .header(JWT_HEADER, jwt)
        )
            .andExpect(status().isOk());
    }

    private ResultActions login() throws Exception {
        return mockMvc.perform(post("/login")
            .contentType(MediaType.APPLICATION_JSON)
            .content(userAsJson));
    }

    private ResultActions signin() throws Exception {
        return mockMvc.perform(post("/signin")
            .contentType(MediaType.APPLICATION_JSON)
            .content(userAsJson));
    }
}
